package com.example.VegetableShopFinalProjectXml.model;

public class Product {
    private String code;
    private String name;
    private String unit;
    private double price;
    private int quantity;
    private String img;
    private Description description;
    
    public Product() {
    }

    public Product(String code, String name, String unit, double price, int quantity, String img,
            Description description) {
        this.code = code;
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.quantity = quantity;
        this.img = img;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Product [code=" + code + ", description=" + description + ", img=" + img + ", name=" + name + ", price="
                + price + ", quantity=" + quantity + ", unit=" + unit + "]";
    }

    
}
