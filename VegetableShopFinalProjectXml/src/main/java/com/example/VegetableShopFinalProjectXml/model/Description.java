package com.example.VegetableShopFinalProjectXml.model;

public class Description {
    private String trademark;
    private double weight;
    private String type;
    private String status;
    private String detail;

    public Description() {
    }

    public Description(String trademark, double weight, String unit, String status, String detail) {
        this.trademark = trademark;
        this.weight = weight;
        this.type = unit;
        this.status = status;
        this.detail = detail;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getType() {
        return type;
    }

    public void setType(String unit) {
        this.type = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Description [detail=" + detail + ", status=" + status + ", trademark=" + trademark + ", type=" + type
                + ", weight=" + weight + "]";
    }
    
    
    
}
