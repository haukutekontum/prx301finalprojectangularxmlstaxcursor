package com.example.VegetableShopFinalProjectXml.service;

import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.xml.stream.*;

import com.example.VegetableShopFinalProjectXml.model.Description;
import com.example.VegetableShopFinalProjectXml.model.Product;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductHandleService {
    public static List<Product> readListProducts() throws FileNotFoundException, XMLStreamException {
        List<Product> productList = new ArrayList<>();
        Product product = null;
        String tagContent = null;
        Description description = null;

        File file = ResourceUtils.getFile("product.xml");
        InputStream inputStream = new FileInputStream(file);
        XMLInputFactory factory = XMLInputFactory.newFactory();
        XMLStreamReader reader = factory.createXMLStreamReader(inputStream);

        while (reader.hasNext()) {
            int event = reader.next();

            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    if ("product".equals(reader.getLocalName())) {
                        product = new Product();
                        product.setCode(reader.getAttributeValue(0));
                    }
                    if ("description".equals(reader.getLocalName())) {
                        description = new Description();
                    }
                    if("price".equals(reader.getLocalName())){
                        product.setUnit(reader.getAttributeValue(0));
                    }
                    if("weight".equals(reader.getLocalName())){
                        description.setType(reader.getAttributeValue(0));
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    tagContent = reader.getText().trim();
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "product":
                            productList.add(product);
                            break;
                        case "name":
                            product.setName(tagContent);
                            break;
                        case "price":
                            product.setPrice(Double.parseDouble(tagContent));
                            break;
                        case "quantity":
                            product.setQuantity(Integer.parseInt(tagContent));
                            break;
                        case "img":
                            product.setImg(tagContent);
                            break;
                        case "trademark":
                            description.setTrademark(tagContent);
                            break;
                        case "weight":
                            description.setWeight(Double.parseDouble(tagContent));
                            break;
                        case "status":
                            description.setStatus(tagContent);
                            break;
                        case "detail":
                            description.setDetail(tagContent);
                            break;
                        case "description":
                            product.setDescription(description);
                            break;
                    }
                    break;
            }
        }
        return productList;
    }

    public static Product addProduct(Product product) throws XMLStreamException, FileNotFoundException {
        List<Product> productList = readListProducts();
        productList.add(product);
        productList = updateStatusProduct(productList);
        saveDataToXmlFile(productList, "product");
        return product;
    }

    public static Product updateProduct(Product product) throws XMLStreamException, FileNotFoundException {
        List<Product> productList = readListProducts();
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getCode().equalsIgnoreCase(product.getCode())) {
                productList.set(i, product);
                break;
            }
        }
        productList = updateStatusProduct(productList);
        saveDataToXmlFile(productList, "product");
        return product;
    }

    public static void deleteProduct(String id) throws XMLStreamException, FileNotFoundException {
        List<Product> productList = readListProducts();

        for (Product product : productList) {
            if (product.getCode().equalsIgnoreCase(id)) {
                productList.remove(product);
                break;
            }
        }

        saveDataToXmlFile(productList, "product");
    }

    public static Product getProductById(String id) throws XMLStreamException, FileNotFoundException {
        List<Product> productList = readListProducts();

        for (Product product : productList) {
            if (product.getCode().equalsIgnoreCase(id)) {
                return product;
            }
        }
        return null;
    }
    private static void saveDataToXmlFile(List<Product> productList, String fileName) {
        try {
            XMLOutputFactory output = XMLOutputFactory.newInstance();
            File file = ResourceUtils.getFile(fileName + ".xml");
            XMLStreamWriter writer = output.createXMLStreamWriter(new FileOutputStream(file));
            writer.writeStartDocument(); // start document
            writer.writeStartElement("products");
            for (Product product : productList) {
                writer.writeStartElement("product");
                writer.writeAttribute("code", product.getCode());

                writer.writeStartElement("name");
                writer.writeCharacters(product.getName());
                writer.writeEndElement();

                writer.writeStartElement("price");
                writer.writeAttribute("unit", product.getUnit());
                writer.writeCharacters(String .valueOf(product.getPrice()));
                writer.writeEndElement();

                writer.writeStartElement("quantity");
                writer.writeCharacters(String.valueOf(product.getQuantity()));
                writer.writeEndElement();

                writer.writeStartElement("img");
                writer.writeCharacters(product.getImg());
                writer.writeEndElement();

                writer.writeStartElement("description");

                writer.writeStartElement("trademark");
                writer.writeCharacters(product.getDescription().getTrademark());
                writer.writeEndElement();

                writer.writeStartElement("weight");
                writer.writeAttribute("type", product.getDescription().getType());
                writer.writeCharacters(String.valueOf(product.getDescription().getWeight()));
                writer.writeEndElement();

                writer.writeStartElement("status");
                writer.writeCharacters(product.getDescription().getStatus());
                writer.writeEndElement();

                writer.writeStartElement("detail");
                writer.writeCharacters(product.getDescription().getDetail());
                writer.writeEndElement();

                writer.writeEndElement();
                writer.writeEndElement();

            }
            writer.writeEndElement(); //
            writer.writeEndDocument(); // end document

            writer.flush();
            writer.close();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Product> updateStatusProduct(List<Product> productList){
        for(int i=0; i<productList.size(); i++){
            if(productList.get(i).getQuantity()<=0){
                productList.get(i).getDescription().setStatus("Out of stock");
            }else{
                productList.get(i).getDescription().setStatus("Stocking");
            }
        }
        return productList;
    }
}
