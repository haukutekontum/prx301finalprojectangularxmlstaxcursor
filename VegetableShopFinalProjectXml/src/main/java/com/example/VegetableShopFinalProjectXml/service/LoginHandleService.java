package com.example.VegetableShopFinalProjectXml.service;

import com.example.VegetableShopFinalProjectXml.model.Description;
import com.example.VegetableShopFinalProjectXml.model.User;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.xml.stream.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class LoginHandleService {
    public static List<User> readListUsers() throws FileNotFoundException, XMLStreamException {
        List<User> userList = new ArrayList<>();
        User user = null;
        String tagContent = null;

        File file = ResourceUtils.getFile("login.xml");
        InputStream inputStream = new FileInputStream(file);
        XMLInputFactory factory = XMLInputFactory.newFactory();
        XMLStreamReader reader = factory.createXMLStreamReader(inputStream);

        while (reader.hasNext()) {
            int event = reader.next();

            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    if ("user".equals(reader.getLocalName())) {
                        user = new User();
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    tagContent = reader.getText().trim();
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "user":
                            userList.add(user);
                            break;
                        case "login":
                            user.setLogin(tagContent);
                            break;
                        case "password":
                            user.setPassword(tagContent);
                            break;
                    }
                    break;
            }
        }
        return userList;
    }

    public static User addUser(User user) throws XMLStreamException, FileNotFoundException {
        List<User> userList = readListUsers();
        userList.add(user);
        saveDataToXmlFile(userList, "login");
        return user;
    }

    public static User login(User userlo) throws XMLStreamException, FileNotFoundException {
        List<User> userList = readListUsers();

        for (User user : userList) {
            if (user.getLogin().equalsIgnoreCase(userlo.getLogin())) {
                if (user.getPassword().equalsIgnoreCase(userlo.getPassword())) {
                    return user;
                }
            }
        }
        return null;
    }

    private static void saveDataToXmlFile(List<User> userList, String fileName) {
        try {
            XMLOutputFactory output = XMLOutputFactory.newInstance();
            File file = ResourceUtils.getFile(fileName + ".xml");
            XMLStreamWriter writer = output.createXMLStreamWriter(new FileOutputStream(file));
            writer.writeStartDocument(); // start document
            writer.writeStartElement("users");
            for (User user : userList) {
                writer.writeStartElement("user");

                writer.writeStartElement("login");
                writer.writeCharacters(user.getLogin());
                writer.writeEndElement();

                writer.writeStartElement("password");
                writer.writeCharacters(user.getPassword());
                writer.writeEndElement();

                writer.writeEndElement();

            }
            writer.writeEndElement(); //
            writer.writeEndDocument(); // end document

            writer.flush();
            writer.close();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
