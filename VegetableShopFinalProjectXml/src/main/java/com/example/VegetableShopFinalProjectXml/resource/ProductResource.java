package com.example.VegetableShopFinalProjectXml.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.xml.stream.XMLStreamException;

import com.example.VegetableShopFinalProjectXml.model.Product;
import com.example.VegetableShopFinalProjectXml.service.ProductHandleService;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ProductResource {
    @Autowired
    private ProductHandleService productHandleService;

    @GetMapping("/all")
    public ResponseEntity<List<Product>> getAllProducts () throws  FileNotFoundException, XMLStreamException {
        List<Product> productList = ProductHandleService.readListProducts();
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }

    @GetMapping("/find/{code}")
    public ResponseEntity<Product> getProductByCode (@PathVariable("code") String id) throws FileNotFoundException, XMLStreamException {
        Product product = ProductHandleService.getProductById(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Product> addProduct(@RequestBody Product product) throws FileNotFoundException, XMLStreamException {
        Product newProduct = ProductHandleService.addProduct(product);
        return new ResponseEntity<>(newProduct, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Product> updateProduct(@RequestBody Product product) throws FileNotFoundException, XMLStreamException {
        Product updateProduct = ProductHandleService.updateProduct(product);
        return new ResponseEntity<>(updateProduct, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{code}")
    public ResponseEntity<?> deleteProduct(@PathVariable("code") String code) throws FileNotFoundException, XMLStreamException {
        ProductHandleService.deleteProduct(code);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
