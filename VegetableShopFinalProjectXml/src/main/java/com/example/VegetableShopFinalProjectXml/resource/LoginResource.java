package com.example.VegetableShopFinalProjectXml.resource;

import com.example.VegetableShopFinalProjectXml.model.Product;
import com.example.VegetableShopFinalProjectXml.model.User;
import com.example.VegetableShopFinalProjectXml.service.LoginHandleService;
import com.example.VegetableShopFinalProjectXml.service.ProductHandleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.util.List;

@Controller
public class LoginResource {
    @Autowired
    private LoginHandleService loginHandleService;

    @GetMapping("/allUser")
    public ResponseEntity<List<User>> getAllProducts () throws  FileNotFoundException, XMLStreamException {
        List<User> userList = loginHandleService.readListUsers();
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }

    @PostMapping("/createUser")
    public ResponseEntity<User> addUser(@RequestBody User user) throws FileNotFoundException, XMLStreamException {
        User newUser = loginHandleService.addUser(user);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<User> login (@RequestBody User user) throws FileNotFoundException, XMLStreamException {
        User userNew = loginHandleService.login(user);
        return new ResponseEntity<>(userNew, HttpStatus.OK);
    }

}
