
export interface Product{
  code: string | undefined;
  name: string;
  unit: string;
  price: number;
  quantity: number;
  img: string;
  description:Description;
}

export interface Description{
  trademark: string;
  weight: number;
  type: string;
  status: string;
  detail: string;
}
