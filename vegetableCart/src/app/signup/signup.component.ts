import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Description, Product} from "../services/product";
import {ProductService} from "../services/product.service";
import {User} from "../services/user";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public user?: User | null;
  public confirmPassword ?: string | null;

  constructor(private http: HttpClient,
              private producService: ProductService,
              private router: Router) {

  }

  ngOnInit(): void {
  }

  public onSignUp(signup: NgForm): void {
      this.producService.addUser(signup.value).subscribe(
        (response: User) => {
          console.log(response);
          alert("Sign Up Successfull");
          signup.reset();
          this.router.navigate(['login']);
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }

}
