import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../services/product.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Description, Product} from "../../services/product";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {
  public products?: Product[];
  public product?: Product | null;
  public addProductDescription?: Description | null;
  public editProduct?: Product | null;
  public editProductDescription?: Description;
  public deleteProduct?: Product | null;

  public unitPrice = ['VND', 'USD', 'YEN'];
  public selectedUnitPrice: string = 'VND';

  constructor(private productService: ProductService) {
  }

  ngOnInit(): void {
    this.getProducts();
  }

  public getProducts(): void {
    this.productService.getProducts().subscribe(
      (response: Product[]) => {
        this.products = response;
        console.log(this.products);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddProduct(addForm: NgForm, product: Product, addDescriptionForm: NgForm, description: Description): void {
    document.getElementById('add-product-form')?.click();
    product.description = description;
    product.unit = 'VND';
    product.description.type = 'kg';
    this.productService.addProduct(product).subscribe(
      (response: Product) => {
        console.log(response);
        this.getProducts();
        addForm.reset();
        addDescriptionForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
        addDescriptionForm.reset();
      }
    );
  }

  public onDeleteProduct(productCode: string | undefined): void {
    if (typeof (productCode) === 'string') {
      this.productService.deleteProduct(productCode).subscribe(
        (response: void) => {
          console.log(response);
          this.getProducts();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
  }

  public onUpdateProduct(product: Product, description: Description): void {
    product.description = description;
    this.productService.updateProduct(product).subscribe(
      (response: Product) => {
        console.log(response);
        this.getProducts();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onOpenModal(product: Product | null, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addProductModal');
    }
    if (mode === 'edit') {
      this.editProduct = product;
      this.editProductDescription = product?.description;
      button.setAttribute('data-target', '#updateProductModal');
    }
    if (mode === 'delete') {
      this.deleteProduct = product;
      button.setAttribute('data-target', '#deleteProductModal');
    }
    // @ts-ignore
    container.append(button);
    button.click();
  }

  public searchProducts(key: string): void {
    console.log(key);
    const results: Product[] = [];
    if (this.products) {
      for (const product of this.products) {
        if (product.name.toLowerCase().indexOf(key.toLowerCase()) !== -1
          || product.unit.toLowerCase().indexOf(key.toLowerCase()) !== -1
          || product.description.status.toLowerCase().indexOf(key.toLowerCase()) !== -1)
          results.push(product);
      }
    }
    this.products = results;
    if (results.length === 0 || !key) {
      this.getProducts();
    }
  }

}
