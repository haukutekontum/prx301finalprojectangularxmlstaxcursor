import { Component, OnInit } from '@angular/core';
import {ProductService} from "../services/product.service";
import {Description, Product} from "../services/product";
import {HttpErrorResponse} from "@angular/common/http";
import {NgForm} from "@angular/forms";
import {User} from "../services/user";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
  }

  public onLogin(loginForm: NgForm): void {
    this.productService.Login(loginForm.value).subscribe(
      (response: User) => {
        alert("Login successful");
        console.log(response);
        this.router.navigate(['product']);

      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

}
